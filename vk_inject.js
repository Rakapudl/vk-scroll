var index;

function next_post() {
	var curEl = $('.feed_row')[index];

	if($(curEl).find('#ads_feed_placeholder').length > 0) {
		index++;
		curEl = $('.feed_row')[index];
	}

	if(curEl) {
		$('html, body').animate({
        	scrollTop: $(curEl).offset().top-50
    	}, 200);
	}
}


$(document).keypress(function(e) {
	if(e.which == 32) {
		var visibleEl = inView('.feed_row').check().current.pop();
		index = visibleEl ? $(visibleEl).index()+1 : index+1;
 		next_post();
  	}
 });



inView.threshold(0.2);
